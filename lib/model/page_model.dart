import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'order_item_model.dart';

class PageModel {
  int total;
  int size;
  int current;
  int pages;
  String queryTime;
  List<OrderItemModel> orders;
  List<Map<String, dynamic>> records;
  String filterType;
  String filterValue;
  List filterList;
  Map filterMap;

  PageModel(
      {this.total = 0,
      this.size = 10,
      this.current = 1,
      this.pages = 1,
        this.queryTime = '',
      this.orders = const [],
      this.records = const [],
      this.filterList = const [],
      this.filterType = '',
      this.filterValue = '',
      this.filterMap = const {}});

  PageModel copyWith(
      {int? total,
      int? size,
      int? current,
      int? pages,
        String? queryTime,
      List? filterList,
      List<OrderItemModel>? orders,
      List<Map<String, dynamic>>? records,
      String? filterType,
      String? filterValue,
      Map? filterMap}) {
    return PageModel(
      total: total ?? this.total,
      size: size ?? this.size,
      current: current ?? this.current,
      pages: pages ?? this.pages,
      queryTime : queryTime ?? this.queryTime,
      orders: orders ?? this.orders,
      filterList: filterList ?? this.filterList,
      records: records ?? this.records,
      filterMap: filterMap ?? this.filterMap,
      filterType: filterType ?? this.filterType,
      filterValue: filterValue ?? this.filterValue,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'total': total,
      'size': size,
      'current': current,
      'pages': pages,
      'queryTime' : queryTime,
      'filterMap': filterMap,
      'filterList': filterList,
      'orders': orders.map((x) => x.toMap()).toList(),
      'records': records,
      'filterType': filterType,
      'filterValue': filterValue,
    };
  }

  factory PageModel.fromMap(Map<String, dynamic> map) {
    return PageModel(
      total: map['total'],
      size: map['size'],
      current: map['current'],
        queryTime : map['queryTime'],
      pages: map['pages'],
      filterMap: map['filterMap'],
      filterList: map['filterList'],
      orders: map['orders'] == null
          ? []
          : List<OrderItemModel>.from(
              map['orders'].map((x) => OrderItemModel.fromMap(x))),
      records: List.from(map['records'].map((x) => x)),
      filterType: map['filterType'],
      filterValue: map['filterValue'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PageModel.fromJson(String source) =>
      PageModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PageModel(total: $total, queryTime:$queryTime, filterMap: $filterMap,filterType:$filterType,filterValue: $filterValue,filterList :$filterList, size: $size, current: $current, pages: $pages, orders: $orders, records: $records)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is PageModel &&
        o.total == total &&
        o.size == size &&
        o.current == current &&
        o.queryTime == queryTime &&
        o.filterMap == filterMap &&
        o.filterList == filterList &&
        o.pages == pages &&
        o.filterValue == filterValue &&
        o.filterType == filterType &&
        listEquals(o.orders, orders) &&
        listEquals(o.records, records);
  }

  @override
  int get hashCode {
    return total.hashCode ^
        size.hashCode ^
        current.hashCode ^
        queryTime.hashCode ^
        pages.hashCode ^
        orders.hashCode ^
        filterValue.hashCode ^
        filterType.hashCode ^
        filterMap.hashCode ^
        records.hashCode;
  }
}
