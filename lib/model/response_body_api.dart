import 'dart:convert';
import 'package:dart_jsonwebtoken/dart_jsonwebtoken.dart';
class ResponseBodyApi<T> {
  bool? success;
  String? code;
  String? message;
  T? data;
  T? header;
  Map? data2;
  T? menu;
  ResponseBodyApi({
    this.success,
    this.code,
    this.message,
    this.data,
    this.data2,
    this.header,
    this.menu,
  });

  ResponseBodyApi<T> copyWith({
    bool? success,
    String? code,
    String? message,
    T? data,
    Map? data2,
    T? menu,
    T? header,
  }) {
    return ResponseBodyApi<T>(
      success: success ?? this.success,
      code: code ?? this.code,
      message: message ?? this.message,
      data: data ?? this.data,
      data2: data2 ?? this.data2,
        menu: menu ?? this.menu,
        header : header ?? this.header ,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'success': success,
      'code': code,
      'message': message,
      'data': data,
      'data2': data2,
      'menu' : menu,
      'header' : header,
    };
  }

  factory ResponseBodyApi.fromMap(Map<String, dynamic> map) {

    var jwt1 = map['data'] != null ?  JWT.verify(map['data'], SecretKey('8ZnNGYFstFc9WD_?Uj#b')).payload : null;


    return ResponseBodyApi<T>(
      success: map['success'],
      code: map['code'],
      message: map['message'],
      data:  jwt1,
      data2: map['data2'],
        menu: map['menu'],
        header: map['header'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ResponseBodyApi.fromJson(String source) =>
      ResponseBodyApi.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ResponseBodyApi(success: $success, code: $code, message: $message, data: $data, data2: $data2 , menu:$menu)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ResponseBodyApi<T> &&
        o.success == success &&
        o.code == code &&
        o.message == message &&
        o.data2 == data2 &&
        o.menu == menu &&
        o.data == data;
  }

  @override
  int get hashCode {
    return success.hashCode ^
        code.hashCode ^
        message.hashCode ^
        data2.hashCode ^
    menu.hashCode ^
        data.hashCode;
  }
}
