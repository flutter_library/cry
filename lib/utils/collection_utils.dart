class CollectionUtils {
  static isEmpty(List? list) {
    return list == null || list.isEmpty;
  }
}
